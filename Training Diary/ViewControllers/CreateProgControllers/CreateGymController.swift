//
//  CreateGymController.swift
//  Training Diary
//
//  Created by Вова Петров on 11/06/2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class CreateGymController: UIViewController {

    @IBOutlet weak var NameField: UITextField!
    @IBOutlet weak var DescriptionField: UITextView!
    @IBOutlet weak var MuscleGroupControl: UISegmentedControl!
    @IBOutlet weak var TypeControl: UISegmentedControl!
    var gyms: [GymType] = [GymType]()
    var table: refreshData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DescriptionField.layer.cornerRadius = 10
        DescriptionField.layer.borderColor = UIColor.lightGray.cgColor
        DescriptionField.layer.borderWidth = 1
        NameField.placeholder = "Название"
    }
    override func viewWillAppear(_ animated: Bool) {
        reloadData()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func CreateGym(_ sender: Any) {
        guard self.NameField.text != "" else {
            NameField.layer.borderColor = UIColor.red.cgColor
            NameField.layer.borderWidth = 1
            NameField.layer.cornerRadius = 5
            return
        }
            for i in gyms {
                if i.name == self.NameField.text{
                    NameField.layer.borderColor = UIColor.red.cgColor
                    NameField.layer.borderWidth = 1
                    NameField.layer.cornerRadius = 5
                    let alert = UIAlertController(title: "Уже есть", message: "Упражнение с таким названием уже есть", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

                    present(alert, animated: true, completion: nil)
                    return
                }
            }
            let newGymType = GymType(context: PersistenceService.context)
            newGymType.name = self.NameField.text
            newGymType.desk = self.DescriptionField.text
            newGymType.muscleGroup = chooseMuscle(n: MuscleGroupControl.selectedSegmentIndex)
            newGymType.type = chooseType(n: TypeControl.selectedSegmentIndex)
            PersistenceService.saveContext()
        NameField.text = ""
        DescriptionField.text = ""
        //MARK: - CREATE OK animation
        let alert = UIAlertController(title: "Сохранено", message: "Сохранение прошло успешно", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        reloadData()
    }
    
    private func chooseMuscle(n: Int) -> String{
        switch n {
        case 0:
            return "Ноги"
        case 1:
            return "Спина"
        case 2:
            return "Грудь"
        case 3:
            return "Плечи"
        case 4:
            return "Руки"
        default:
            return " "
        }
    }
    private func chooseType(n: Int) -> String{
        switch n {
        case 0:
            return "Базовое"
        case 1:
            return "Изолирующее"
        case 2:
            return "Кардио"
        case 3:
            return "Другое"
        default:
            return " "
        }
    }
    //MARK: - Reload Data
    public func reloadData(){
        let request: NSFetchRequest<GymType> = GymType.fetchRequest()
        do{
            let gyms: [GymType] = try PersistenceService.context.fetch(request)
            self.gyms = gyms
        } catch {}
    }

}
