//
//  NewProgrammViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 14.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit


class NewProgrammViewController: UIViewController, setModel {

    @IBOutlet weak var desctiptionTextView: UITextView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dayTable: UITableView!
    var progModel: CreateProgModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progModel = CreateProgModel.init()
        navigationController!.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
        nameField.placeholder = "Название программы"
        desctiptionTextView.layer.cornerRadius = 10
        desctiptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        desctiptionTextView.layer.borderWidth = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dayTable.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func createDayButton(_ sender: Any) {
        performSegue(withIdentifier: "NewDay", sender: self)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewDay" {
            let destVC = segue.destination as! UINavigationController
            (destVC.viewControllers[0] as! NewDayViewController).progController = self
            (destVC.viewControllers[0] as! NewDayViewController).progModel = progModel!
        } else if segue.identifier == "DetailDay" {
            let destVC = segue.destination as! DetailDayViewController
            
            if let indexPath = dayTable.indexPathForSelectedRow {
                var gymTypes: [GymType] = [GymType]()
                for i in (progModel?.days[indexPath.row].toOneGym?.allObjects as! [OneGym]){
                    gymTypes.append(i.gymToType!)
                }
                destVC.gyms = gymTypes
                destVC.name = (progModel?.days[indexPath.row].name!)!
            }
        }
        
    }
    
    func setModel(model: CreateProgModel) {
        self.progModel = model
    }
    @IBAction func saveProg(_ sender: Any) {
        if nameField.text == nil || nameField.text == "" {
            nameField.layer.borderColor = UIColor.red.cgColor
            nameField.layer.borderWidth = 1
            nameField.layer.cornerRadius = 5
        }else if progModel!.days.count == 0 {
            nameField.layer.borderWidth = 0
            dayTable.layer.borderColor = UIColor.red.cgColor
            dayTable.layer.borderWidth = 1
            dayTable.layer.cornerRadius = 5
        } else{
        self.progModel?.createAndSaveFullProg(name: nameField.text!, desk: desctiptionTextView.text)
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        PersistenceService.context.delete((progModel?.prog)!) 
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension NewProgrammViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return progModel?.days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView.init()
        v.backgroundColor? = UIColor.clear
        return v;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! NewProgCellTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if progModel?.days.count != 0{
        let cell = cell as! NewProgCellTableViewCell
        cell.name.text = progModel?.days[indexPath.row].name!
            cell.dayCount.text = "Упражнений: \(String((progModel?.days[indexPath.row].toOneGym?.count)!))"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gym = progModel?.days[indexPath.row].toOneGym
        performSegue(withIdentifier: "DetailDay", sender: gym)
    }

    
    
}
