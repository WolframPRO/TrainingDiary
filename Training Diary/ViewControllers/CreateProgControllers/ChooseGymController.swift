//
//  ChooseGymController.swift
//  Training Diary
//
//  Created by Вова Петров on 14.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit
import CoreData
import Foundation

protocol refreshData {
    func reloadData() -> Void
}
protocol cellIsActive {
    func thisCellActive(name: String)
    func thisCellDeactive(name: String)
}

class ChooseGymController: UIViewController {
   
    @IBOutlet weak var GymTable: UITableView!
    var gyms: [GymType] = [GymType]()
    var filteredGyms: [GymType] = [GymType]()
    var selected: [String] = [String]()
    var dayController: setGyms?
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        searchController.searchResultsUpdater = self
        
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
    }
    override func viewWillAppear(_ animated: Bool) {
        reloadData()
    }
   

    @IBAction func CreateGymClick(_ sender: Any) {
        performSegue(withIdentifier: "NewGym", sender: nil)
    }
    
    
}
extension ChooseGymController: UITableViewDelegate, UITableViewDataSource, refreshData, cellIsActive, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filteredGyms = gyms.filter({ (gym) -> Bool in
            gym.name!.contains(searchController.searchBar.text!)
        })
        
       GymTable.reloadData()
    }
    
    func thisCellDeactive(name: String) {
        for i in 0..<self.selected.count{
            if self.selected[i] == name{
        self.selected.remove(at: i)
            }
        }
    }
    
    func thisCellActive(name: String) {
        self.selected.append(name)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredGyms.count
        }
        return gyms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ChooseGymTableCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configureCell(cell: cell as! ChooseGymTableCell, for: indexPath)
    }
     private func configureCell(cell: ChooseGymTableCell, for indexPath: IndexPath){
        let gym: GymType
        gym = searchController.isActive ? filteredGyms[indexPath.row] : gyms[indexPath.row]
        
        for i in 0..<selected.count{
            if gym.name == selected[i]{
                cell.OnOff.isOn = true
                break
            } else {
                cell.OnOff.isOn = false
            }
        }
        
        cell.NameLabel.text = gym.name
        cell.MuscleLabel.text = gym.muscleGroup
        cell.TypeLabel.text = gym.type
        cell.index = indexPath
        cell.Table = self
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Удалить") {
            _, indexPath in
            let gym = self.gyms.remove(at: indexPath.row)
            PersistenceService.context.delete(gym)
            tableView.deleteRows(at: [indexPath], with: .fade)
            PersistenceService.saveContext()
        }
        
        return [deleteAction]
    }
    

    
    @IBAction func createDayDone(_ sender: Any) {
        var selectedGyms = [GymType]()
        
        for i in 0..<selected.count{
            for n in 0..<gyms.count{
                if selected[i] == gyms[n].name{
                    selectedGyms.append(gyms[n])
                    break
                }
            }
        }
        dayController?.setGyms(gyms: selectedGyms)
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - Reload Data
    public func reloadData(){
        let request: NSFetchRequest<GymType> = GymType.fetchRequest()
        do{
            let gyms: [GymType] = try PersistenceService.context.fetch(request)
            self.gyms = gyms
        } catch {}
        GymTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gym = searchController.isActive ? filteredGyms[indexPath.row] : gyms[indexPath.row]
        performSegue(withIdentifier: "DetailGym", sender: gym)
    }
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DetailGym" {
            let destVC = segue.destination as! DetailChooseGymController
            
            if let indexPath = GymTable.indexPathForSelectedRow {
                destVC.detailGym = searchController.isActive ? filteredGyms[indexPath.row] : gyms[indexPath.row]
            }
        }
    }

}
