//
//  ChooseProgController.swift
//  Training Diary
//
//  Created by Вова Петров on 15.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class ChooseProgController: UIViewController {
   
    @IBOutlet weak var progTable: UITableView!
    var progs: [TrainingProg]?
    var chooseProg: TrainingProg?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progTable.layer.masksToBounds = false
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        reloadData()
    }
    @IBAction func doneClick(_ sender: Any) {
        if self.chooseProg != nil{
        let defaults = UserDefaults.standard
        defaults.set(self.chooseProg?.name, forKey: "prog")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ChooseProgController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return progs?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView.init()
        v.backgroundColor? = UIColor.clear
        return v;
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView.init()
        v.backgroundColor? = UIColor.clear
        return v;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ChooseProgCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! ChooseProgCell
        cell.name.text = progs![indexPath.section].name
        cell.desk.text = progs![indexPath.section].desk
        cell.days.text = "Дней: \(progs![indexPath.section].dayCount)"
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Удалить") {
            _, indexPath in
            let delProg = self.progs!.remove(at: indexPath.section)
            PersistenceService.context.delete(delProg)
            tableView.deleteSections([indexPath.section], with: .automatic)
            PersistenceService.saveContext()
        }
        let chooseAction: UITableViewRowAction = UITableViewRowAction(style: .default, title: "Выбрать") {
            _, indexPath in
            self.chooseProg = self.progs![indexPath.section]
        }
        chooseAction.backgroundColor = UIColor.blue
        return [chooseAction, deleteAction]
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let days = progs?[indexPath.section].dayOnProg?.allObjects as! [TrainingDay]
        performSegue(withIdentifier: "DetailProg", sender: days)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailProg" {
            let destVC = segue.destination as! DetailProgViewController
            
            if let indexPath = progTable.indexPathForSelectedRow {
                var days: [TrainingDay] = [TrainingDay]()
                for i in (progs?[indexPath.section].dayOnProg!.allObjects as! [TrainingDay]){
                    days.append(i)
                }
                destVC.days = days
                destVC.name = (progs?[indexPath.section].name)!
            }
        }
    }
    
    public func reloadData(){
        let request: NSFetchRequest<TrainingProg> = TrainingProg.fetchRequest()
        do{
            let progs: [TrainingProg] = try PersistenceService.context.fetch(request)
            self.progs = progs
        } catch {}
        progTable.reloadData()
    }
    
}
