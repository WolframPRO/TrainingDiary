//
//  NewDayViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 14.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit


protocol setModel {
    func setModel(model: CreateProgModel)
}

protocol setGyms {
    func setGyms(gyms: [GymType])
}

class NewDayViewController: UIViewController, setModel, setGyms {

    @IBOutlet weak var gymsTable: UITableView!
    var progModel: CreateProgModel?
    var progController: setModel?
    var gyms: [GymType]?
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameField.placeholder = "Название дня"
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
        
        descriptionTextView.layer.cornerRadius = 10
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        gymsTable.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func setModel(model: CreateProgModel){
        self.progModel = model
    }
    func setGyms(gyms: [GymType]) {
        self.gyms = gyms
    }
    
    @IBAction func startEditing(_ sender: Any) {
        if !gymsTable.isEditing {
        gymsTable?.setEditing(true, animated: true)
        } else {
            gymsTable?.setEditing(false, animated: true)
        }
    }
    

    
    //MARK: - Choose Gym Click
    @IBAction func chooseClick(_ sender: Any) {
        performSegue(withIdentifier: "ShowChooseGym", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChooseGym" {
            let destVC = segue.destination as! UINavigationController
            (destVC.viewControllers[0] as! ChooseGymController).dayController = self
        } else if segue.identifier == "ShowType"{
            let destVC = segue.destination as! DetailChooseGymController
            
            if let indexPath = gymsTable.indexPathForSelectedRow {
                destVC.detailGym = gyms![indexPath.row]
            }
        }
        
    }
    @IBAction func cansel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveDay(_ sender: Any) {
        if nameField.text == nil || nameField.text == ""  {
            nameField.layer.borderColor = UIColor.red.cgColor
            nameField.layer.borderWidth = 1
            nameField.layer.cornerRadius = 5
        } else if gyms?.count == 0 || gyms == nil{
            gymsTable.layer.borderColor = UIColor.red.cgColor
            gymsTable.layer.borderWidth = 1
            gymsTable.layer.cornerRadius = 5
        }else{
        _ = progModel!.createFullDay(name: nameField.text!, number: progModel!.days.count, sortedGyms: gyms!)
        progController?.setModel(model: progModel!)
        self.dismiss(animated: true, completion: nil)
        }
    }

}



extension NewDayViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gyms?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel!.text = gyms?[indexPath.row].name ?? ""
        cell.detailTextLabel!.text = "\(String(gyms![indexPath.row].type!)) : \(String(gyms![indexPath.row].muscleGroup!))"
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        gyms!.insert(gyms!.remove(at: sourceIndexPath.row), at: destinationIndexPath.row)
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowType", sender: gyms![indexPath.row])
    }
    
    
}
