//
//  CheckProgressViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 21.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit
import CoreData

class CheckProgressViewController: UIViewController {


    @IBOutlet weak var progNotFound: UILabel!
    @IBOutlet weak var gymTable: UITableView!
    var prog: TrainingProg!
    var gymList: [OneGym] = [OneGym]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let name = UserDefaults.standard.object(forKey: "prog") as? String
        self.prog = nil
        let request: NSFetchRequest<TrainingProg> = TrainingProg.fetchRequest()
        do{
            let progs: [TrainingProg] = try PersistenceService.context.fetch(request)
            for i in progs {
                if i.name == name{
                    self.prog = i
                }
            }
        } catch {}
        gymList.removeAll()
        if let prog = self.prog {
        for day in prog.dayOnProg?.allObjects as! [TrainingDay]{
            for gym in day.toOneGym?.allObjects as! [OneGym]{
                gymList.append(gym)
            }
        }
        gymTable.reloadData()
        progNotFound.isHidden = true
        } else {
            progNotFound.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension CheckProgressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gymList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell.init(style: .subtitle, reuseIdentifier: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let gym = gymList[indexPath.row]
        let gymType = gym.gymToType!
        cell.textLabel!.text = "\(gymType.name!)"
        cell.textLabel!.font = UIFont.systemFont(ofSize: 22)
        cell.detailTextLabel!.text = "Подходов: \(String(gym.gymToApproach!.allObjects.count))"
        cell.detailTextLabel!.font = UIFont.systemFont(ofSize: 17)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if gymList[indexPath.row].gymToApproach?.allObjects.count == 0{
            let alert = UIAlertController(title: "Нет данных", message: "Вы мало тренировались, прогресса еще нет", preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else{
        performSegue(withIdentifier: "showProgress", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProgress" {
            let destVC = segue.destination as! ProgressViewController
            destVC.gym = gymList[(gymTable.indexPathForSelectedRow?.row)!]
        }
    }
}
