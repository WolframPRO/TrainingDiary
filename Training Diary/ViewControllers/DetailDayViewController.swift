//
//  DetailDayViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 18.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class DetailDayViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gyms?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = gyms![indexPath.row].name
        cell.detailTextLabel?.text = "\(String(gyms![indexPath.row].type!)) : \(String(gyms![indexPath.row].muscleGroup!))"
        return cell
    }
    

    @IBOutlet weak var table: UITableView!
    var gyms: [GymType]?
    var name: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.name
        self.navigationItem.title = self.name
        table.reloadData()
        // Do any additional setup after loading the view.
        navigationItem.largeTitleDisplayMode = .automatic
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gym = gyms![indexPath.row]
        performSegue(withIdentifier: "ToGym", sender: gym)
    }
    
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ToGym" {
            let destVC = segue.destination as! DetailChooseGymController
            
            if let indexPath = table.indexPathForSelectedRow {
                destVC.detailGym = gyms![indexPath.row]
            }
        }
    }
    


}
