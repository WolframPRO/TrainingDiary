//
//  DayViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 19.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class DayViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    var day: TrainingDay!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
        self.title = day.name
    }   
    
    @IBAction func save(_ sender: Any) {
        PersistenceService.saveContext()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cansel(_ sender: Any) {
        PersistenceService.context.reset()
        self.dismiss(animated: true, completion: nil)
    }
}

extension DayViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return day.toOneGym!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! DayTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configureCell(cell: (cell as! DayTableViewCell), indexPath: indexPath)
    }
    
    func configureCell(cell: DayTableViewCell, indexPath: IndexPath){
        let gym = day.toOneGym?.allObjects[indexPath.section] as! OneGym
        cell.configureCell(gym: gym, superView: self)
    }
    
    
}

