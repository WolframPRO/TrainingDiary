//
//  DetailChooseGymController.swift
//  Training Diary
//
//  Created by Вова Петров on 17.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class DetailChooseGymController: UIViewController {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var muscleLabel: UILabel!
    @IBOutlet weak var deskLabel: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
        navigationItem.largeTitleDisplayMode = .automatic
    }

    var detailGym: GymType? {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if let detailGym = detailGym {
            if let deskLabel = deskLabel, let typeLabel = typeLabel, let muscleLabel = muscleLabel {
                self.title = detailGym.name
                self.navigationItem.title = detailGym.name
                deskLabel.text = detailGym.desk
                typeLabel.text = detailGym.type
                muscleLabel.text = detailGym.muscleGroup
            }
        }
    }

}


