//
//  DetailProgViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 18.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class DetailProgViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return days?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = days![indexPath.row].name
        cell.detailTextLabel?.text = "Упражнений: \(String(days![indexPath.row].toOneGym!.count))"
        return cell
    }
    
    
    @IBOutlet weak var table: UITableView!
    var days: [TrainingDay]?
    var name: String = ""
    var desk: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.name
        self.navigationItem.title = self.name
        table.reloadData()
        // Do any additional setup after loading the view.
        navigationItem.largeTitleDisplayMode = .automatic
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gym = days![indexPath.row].toOneGym
        performSegue(withIdentifier: "ToDay", sender: gym)
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToDay" {
            let destVC = segue.destination as! DetailDayViewController
            
            if let indexPath = table.indexPathForSelectedRow {
                var gymTypes: [GymType] = [GymType]()
                for i in (days![indexPath.row].toOneGym?.allObjects as! [OneGym]){
                    gymTypes.append(i.gymToType!)
                }
                destVC.gyms = gymTypes
                destVC.name = (days![indexPath.row].name!)
            }
        }
    }
}
