//
//  MainViewController.swift
//  Training Diary
//
//  Created by Вова Петров on 07/06/2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var progNotFound: UILabel!
    @IBOutlet weak var TrainDayTable: UITableView!
    
    var prog: TrainingProg?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        definesPresentationContext = true
        TrainDayTable.layer.masksToBounds = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let name = UserDefaults.standard.object(forKey: "prog") as? String
        self.prog = nil
        let request: NSFetchRequest<TrainingProg> = TrainingProg.fetchRequest()
        do{
            let progs: [TrainingProg] = try PersistenceService.context.fetch(request)
            for i in progs {
                if i.name == name{
                    self.prog = i
                }
            }
        TrainDayTable.reloadData()
            if self.prog == nil{
                progNotFound.isHidden = false
            } else {
                progNotFound.isHidden = true
            }
        } catch {}
    }
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let num = prog?.dayOnProg?.count {
            return num
        } else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MainCell
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configureCell(cell: cell as! MainCell, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "StartTrain", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "StartTrain" {
            let destVC = segue.destination as! UINavigationController
            
            if let indexPath = TrainDayTable.indexPathForSelectedRow {
                var day: TrainingDay = TrainingDay()
                for i in (self.prog?.dayOnProg?.allObjects as! [TrainingDay]){
                    if i.number == indexPath.section{
                        day = i
                    }
                }
                (destVC.viewControllers[0] as! DayViewController).day = day
            }
        }
    }
    
    
    func configureCell(cell: MainCell, for indexPath: IndexPath){
        var day: TrainingDay = TrainingDay()
        for i in (self.prog?.dayOnProg?.allObjects as! [TrainingDay]){
            if i.number == indexPath.section{
                day = i
            }
        }
        
        cell.contentView.layer.cornerRadius = 10.0
        cell.Name.text = day.name
        cell.Number.text = String(indexPath.section+1)
        cell.backgroundColor = UIColor.clear
        cell.layer.masksToBounds = false
        cell.layer.shadowOpacity = 0.25
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shadowColor = UIColor.black.cgColor
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView.init()
        v.backgroundColor? = UIColor.clear
        return v;
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView.init()
        v.backgroundColor? = UIColor.clear
        return v;
    }
}


class MainCell: UITableViewCell{
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Number: UILabel!
}
