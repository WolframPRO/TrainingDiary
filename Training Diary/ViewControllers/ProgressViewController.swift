//
//  ProgressViewController.swift
//  
//
//  Created by Вова Петров on 21.06.2018.
//

import UIKit
import ScrollableGraphView

class ProgressViewController: UIViewController, ScrollableGraphViewDataSource {
    
    @IBOutlet var graphView: ScrollableGraphView!
    var gym: OneGym!
    var aprList = [Approach]()
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        switch plot.identifier {
        case "darkLine":
            return Double(aprList[pointIndex].count)
        case "darkLineDot":
            return Double(aprList[pointIndex].count)
        default:
            return 0
        }
    }
    
    func label(atIndex pointIndex: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        dateFormatter.dateFormat = "dd-MM"
        return dateFormatter.string(from: aprList[pointIndex].date! as Date)
    }
    
    func numberOfPoints() -> Int {
        return aprList.count
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        graphView.dataSource = self
        aprList = gym.gymToApproach?.allObjects as! [Approach]
        aprList.sort(by: { $0.date!.compare($1.date! as Date) == .orderedAscending})
        self.title = gym.gymToType!.name
        createBarGraph()
    }

    private func createBarGraph(){

            // Setup the line plot.
            let linePlot = LinePlot(identifier: "darkLine")
            
            linePlot.lineWidth = 1
            linePlot.lineColor = UIColor.colorFromHex(hexString: "#777777")
            linePlot.lineStyle = ScrollableGraphViewLineStyle.smooth
            
            linePlot.shouldFill = true
            linePlot.fillType = ScrollableGraphViewFillType.gradient
            linePlot.fillGradientType = ScrollableGraphViewGradientType.linear
            linePlot.fillGradientStartColor = UIColor.colorFromHex(hexString: "#555555")
            linePlot.fillGradientEndColor = UIColor.colorFromHex(hexString: "#444444")
            
            linePlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
            
            let dotPlot = DotPlot(identifier: "darkLineDot") // Add dots as well.
            dotPlot.dataPointSize = 2
            dotPlot.dataPointFillColor = UIColor.white
            
            dotPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
            
            // Setup the reference lines.
            let referenceLines = ReferenceLines()
            
            referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
            referenceLines.referenceLineColor = UIColor.white.withAlphaComponent(0.2)
            referenceLines.referenceLineLabelColor = UIColor.white
            
            referenceLines.positionType = .absolute
            // Reference lines will be shown at these values on the y-axis.
            referenceLines.absolutePositions = [10, 20, 25, 30]
            referenceLines.includeMinMax = false
            
            referenceLines.dataPointLabelColor = UIColor.white.withAlphaComponent(0.5)
            
            // Setup the graph
            graphView.backgroundFillColor = UIColor.colorFromHex(hexString: "#333333")
            graphView.dataPointSpacing = 80
            
            graphView.shouldAnimateOnStartup = true
            graphView.shouldAdaptRange = true
            graphView.shouldRangeAlwaysStartAtZero = true
            
            graphView.rangeMax = 50
            
            // Add everything to the graph.
            graphView.addReferenceLines(referenceLines: referenceLines)
            graphView.addPlot(plot: linePlot)
            graphView.addPlot(plot: dotPlot)
        
    }
    



}
