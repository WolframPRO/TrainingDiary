//
//  CreateProgModel.swift
//  Training Diary
//
//  Created by Вова Петров on 14.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class CreateProgModel: NSObject {
    public var prog: TrainingProg
    public var days: [TrainingDay]
    
    override init(){
        prog = TrainingProg.init(context: PersistenceService.context)
        days = [TrainingDay.init(context: PersistenceService.context)]
        days.removeAll()
    }
    
    func createAndSaveFullProg(name: String, desk: String?){
        prog.name = name
        prog.dayCount = Int64(days.count)
        if let desk = desk {
        prog.desk = desk
        }
        for i in days {
            prog.addToDayOnProg(i)
            PersistenceService.saveContext()
        }
    }
    
    func createFullDay(name: String, number: Int, sortedGyms: [GymType]) -> TrainingDay{
        let day = TrainingDay.init(context: PersistenceService.context)
        day.name = name
        day.number = Int64(number)
        
        for i in 0..<sortedGyms.count {
            day.addToToOneGym(createOneGym(gymType: sortedGyms[i], num: (i+1)))
        }
        days.append(day)
        return day
    }
    
    func createOneGym(gymType: GymType, num: Int) -> OneGym{
        let oneGym = OneGym.init(context: PersistenceService.context)
        oneGym.gymToType = gymType
        oneGym.num = Int64(num)
       // oneGym.
        return oneGym
    }

    
}
