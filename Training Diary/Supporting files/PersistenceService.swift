//
//  PersistenceService.swift
//  Training Diary
//
//  Created by Вова Петров on 13.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import Foundation
import CoreData

class PersistenceService {
    
    private init() {}
    
    static var context: NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    // MARK: - Core Data stack
    
    public static var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "Training_Diary")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    public static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}

extension NSManagedObjectContext{
    public func saveThrows() {
        if self.hasChanges{
            do{
                try save()
            } catch let error {
                let nserror = error as NSError
                print("Core Data Error: \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

