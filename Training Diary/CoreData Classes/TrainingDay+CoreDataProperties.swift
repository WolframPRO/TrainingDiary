//
//  TrainingDay+CoreDataProperties.swift
//  
//
//  Created by Вова Петров on 10.08.2018.
//
//

import Foundation
import CoreData


extension TrainingDay {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TrainingDay> {
        return NSFetchRequest<TrainingDay>(entityName: "TrainingDay")
    }

    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var number: Int64
    @NSManaged public var picture: NSData?
    @NSManaged public var lastDate: NSDate?
    @NSManaged public var toOneGym: NSSet?
    @NSManaged public var toTrainingProg: TrainingProg?

}

// MARK: Generated accessors for toOneGym
extension TrainingDay {

    @objc(addToOneGymObject:)
    @NSManaged public func addToToOneGym(_ value: OneGym)

    @objc(removeToOneGymObject:)
    @NSManaged public func removeFromToOneGym(_ value: OneGym)

    @objc(addToOneGym:)
    @NSManaged public func addToToOneGym(_ values: NSSet)

    @objc(removeToOneGym:)
    @NSManaged public func removeFromToOneGym(_ values: NSSet)

}
