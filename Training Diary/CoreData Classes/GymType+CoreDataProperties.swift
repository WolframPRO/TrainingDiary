//
//  GymType+CoreDataProperties.swift
//  
//
//  Created by Вова Петров on 10.08.2018.
//
//

import Foundation
import CoreData


extension GymType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GymType> {
        return NSFetchRequest<GymType>(entityName: "GymType")
    }

    @NSManaged public var desk: String?
    @NSManaged public var method: String?
    @NSManaged public var muscleGroup: String?
    @NSManaged public var name: String?
    @NSManaged public var pulseDefault: Int64
    @NSManaged public var timeDefault: Int64
    @NSManaged public var type: String?
    @NSManaged public var toOneGym: NSSet?

}

// MARK: Generated accessors for toOneGym
extension GymType {

    @objc(addToOneGymObject:)
    @NSManaged public func addToToOneGym(_ value: OneGym)

    @objc(removeToOneGymObject:)
    @NSManaged public func removeFromToOneGym(_ value: OneGym)

    @objc(addToOneGym:)
    @NSManaged public func addToToOneGym(_ values: NSSet)

    @objc(removeToOneGym:)
    @NSManaged public func removeFromToOneGym(_ values: NSSet)

}
