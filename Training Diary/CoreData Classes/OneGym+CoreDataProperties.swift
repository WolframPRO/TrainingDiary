//
//  OneGym+CoreDataProperties.swift
//  
//
//  Created by Вова Петров on 10.08.2018.
//
//

import Foundation
import CoreData


extension OneGym {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OneGym> {
        return NSFetchRequest<OneGym>(entityName: "OneGym")
    }

    @NSManaged public var num: Int64
    @NSManaged public var lastDate: NSDate?
    @NSManaged public var gymToApproach: NSSet?
    @NSManaged public var gymToType: GymType?
    @NSManaged public var toDay: TrainingDay?

}

// MARK: Generated accessors for gymToApproach
extension OneGym {

    @objc(addGymToApproachObject:)
    @NSManaged public func addToGymToApproach(_ value: Approach)

    @objc(removeGymToApproachObject:)
    @NSManaged public func removeFromGymToApproach(_ value: Approach)

    @objc(addGymToApproach:)
    @NSManaged public func addToGymToApproach(_ values: NSSet)

    @objc(removeGymToApproach:)
    @NSManaged public func removeFromGymToApproach(_ values: NSSet)

}
