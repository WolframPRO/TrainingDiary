//
//  Approach+CoreDataProperties.swift
//  
//
//  Created by Вова Петров on 10.08.2018.
//
//

import Foundation
import CoreData


extension Approach {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Approach> {
        return NSFetchRequest<Approach>(entityName: "Approach")
    }

    @NSManaged public var count: Int64
    @NSManaged public var date: NSDate?
    @NSManaged public var numApproach: Int64
    @NSManaged public var time: Int64
    @NSManaged public var lastDate: NSObject?
    @NSManaged public var aprDate: NSDate?
    @NSManaged public var toOneGym: OneGym?

}
