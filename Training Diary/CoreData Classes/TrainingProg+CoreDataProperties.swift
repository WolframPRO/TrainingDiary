//
//  TrainingProg+CoreDataProperties.swift
//  
//
//  Created by Вова Петров on 10.08.2018.
//
//

import Foundation
import CoreData


extension TrainingProg {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TrainingProg> {
        return NSFetchRequest<TrainingProg>(entityName: "TrainingProg")
    }

    @NSManaged public var dayCount: Int64
    @NSManaged public var desk: String?
    @NSManaged public var name: String?
    @NSManaged public var dayOnProg: NSSet?

}

// MARK: Generated accessors for dayOnProg
extension TrainingProg {

    @objc(addDayOnProgObject:)
    @NSManaged public func addToDayOnProg(_ value: TrainingDay)

    @objc(removeDayOnProgObject:)
    @NSManaged public func removeFromDayOnProg(_ value: TrainingDay)

    @objc(addDayOnProg:)
    @NSManaged public func addToDayOnProg(_ values: NSSet)

    @objc(removeDayOnProg:)
    @NSManaged public func removeFromDayOnProg(_ values: NSSet)

}
