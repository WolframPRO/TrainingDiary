//
//  ChooseGymTableCell.swift
//  Training Diary
//
//  Created by Вова Петров on 14.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class ChooseGymTableCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var TypeLabel: UILabel!
    @IBOutlet weak var MuscleLabel: UILabel!
    @IBOutlet weak var OnOff: UISwitch!
    var Table: cellIsActive!
    var index: IndexPath!

    @IBAction func swich(_ sender: Any) {
        if OnOff.isOn {
            Table.thisCellActive(name: NameLabel.text!)
        } else{
            Table.thisCellDeactive(name: NameLabel.text!)
        }
    }
}
