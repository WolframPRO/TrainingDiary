//
//  ApproachViewCell.swift
//  Training Diary
//
//  Created by Вова Петров on 20.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class ApproachViewCell: UIView {

    @IBOutlet weak var tapImage: UIImageView!
    @IBOutlet var view: UIView!
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var oldResultKg: UILabel!
    @IBOutlet weak var oldResultNum: UILabel!
    @IBOutlet weak var resultKg: UILabel!
    @IBOutlet weak var resultNum: UILabel!
    var approach: Approach!
    var gym: OneGym!
    var lastApproach: Approach?
    var superView: UIViewController!
    var mainCell: CreateApproach!
    var num: Int!
    var pressed = false
    
    
    init(gym: OneGym, lastApproach: Approach?, num: Int, superView: UIViewController, mainCell: CreateApproach) {
        PersistenceService.context.refreshAllObjects()
        super.init(frame: CGRect(x: 0, y: 0, width: 320, height: 128))
        nibSetup()
        self.num = num
        self.superView = superView
        self.mainCell = mainCell
        self.gym = gym
        self.lastApproach = lastApproach
        configureCell()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "View", bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell() {
        self.numLabel.text = String(num + 1)
        if let lastApproach = lastApproach {
            self.oldResultKg.text = "\(String(lastApproach.count)) кг"
            self.oldResultNum.text = String(lastApproach.time)
        } else {
            self.oldResultKg.text = "-"
            self.oldResultNum.text = "-"
        }
        self.resultKg.text = "-"
        self.resultNum.text = "-"
    }
    
    func createApproach(num: Int) -> Approach {
        let approach = Approach.init(context: PersistenceService.context)
        approach.toOneGym = gym
        approach.numApproach = Int64(num)
        return approach
    }
    
    @IBAction func createThis(_ sender: Any) {
        guard !pressed else {return }
        let alert = UIAlertController(title: "Новый подход", message: "Введите результат", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (_) -> Void in
            self.pressed = false
        }
        let saveAction = UIAlertAction(title: "Сохранить", style: .default) { (_) -> Void in
            let weightField = alert.textFields?[0]
            let numField = alert.textFields?[1]
            
            guard var weight = weightField!.text else {
                return
            }
            guard var count = numField!.text else {
                return
            }
            guard weight != "" && count != "" else {
                return
            }
            while weight.first == "0"{
                weight.removeFirst()
            }
            while count.first == "0"{
                count.removeFirst()
            }
            self.approach = self.createApproach(num: self.num)
            self.approach.count = Int64(weight)!
            self.approach.time = Int64(count)!
            self.approach.numApproach = Int64(self.num)
            
            let date = Date()
            self.approach.date =  date as NSDate
            self.resultKg.text = "\(weight)кг"
            self.resultNum.text = count
            self.mainCell.newApproachView(num: (self.num+1))
            self.mainCell.addApproach(apr: self.approach)
            self.tapImage.isHidden = true
            self.pressed = true
        }
    
        
        alert.addTextField { (_) -> Void in }
        alert.addTextField { (_) -> Void in }
        alert.textFields![0].placeholder = "вес"
        alert.textFields![0].keyboardType = UIKeyboardType.numberPad
        alert.textFields![1].placeholder = "результат"
        alert.textFields![1].keyboardType = UIKeyboardType.numberPad
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        superView.present(alert, animated: true, completion: nil)
    }
}
