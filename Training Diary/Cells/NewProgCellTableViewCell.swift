//
//  NewProgCellTableViewCell.swift
//  Training Diary
//
//  Created by Вова Петров on 18.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class NewProgCellTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dayCount: UILabel!
}
