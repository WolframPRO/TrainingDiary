//
//  DayTableViewCell.swift
//  Training Diary
//
//  Created by Вова Петров on 19.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

protocol CreateApproach {
    func newApproachView(num: Int)
    func addApproach(apr: Approach)
}

class DayTableViewCell: UITableViewCell, CreateApproach {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var View: UIView!
    @IBOutlet weak var WidthOfView: NSLayoutConstraint!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var TypeLabel: UILabel!
    @IBOutlet weak var MuscleLabel: UILabel!
    var gym: OneGym!
    var approachList: [Approach] = [Approach]()
    var lastApproachList: [Approach] = [Approach]()
    var superView: UIViewController!
    
    fileprivate func findLastApproachList() {
        var aprList = gym.gymToApproach?.allObjects as! [Approach]
        aprList.sort(by: { $0.date!.compare($1.date! as Date) == .orderedAscending})
        var lastFirst = 0
        
        for i in 0..<aprList.count {
            if aprList[i].numApproach == 0{
                lastFirst = i
            }
        }
        lastApproachList = [Approach]()
        for i in lastFirst..<aprList.count {
            lastApproachList.append(aprList[i])
        }
    }
    
    func configureCell(gym: OneGym, superView: UIViewController){
        self.gym = gym
        self.superView = superView
        let gymType = gym.gymToType!
        NameLabel.text = gymType.name
        TypeLabel.text = gymType.type
        MuscleLabel.text = gymType.muscleGroup
        findLastApproachList()
        newApproachView(num: 0)
    }
    
    func newApproachView(num: Int) {
        var aprView = ApproachViewCell.init (gym: self.gym, lastApproach: nil, num: num, superView: superView, mainCell: self)
        if lastApproachList.count > num {
            aprView = ApproachViewCell.init(gym: self.gym, lastApproach: lastApproachList[num], num: num, superView: superView, mainCell: self)
        }
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        WidthOfView.constant = CGFloat(Int(screenWidth) * (num + 2))

        aprView.frame = CGRect(x: (Int(screenWidth) + Int(screenWidth) * num), y: 0, width: Int(screenWidth), height: 128)
        
        View.addSubview(aprView)
        //aprView.configureCell()
    }
    
    func addApproach(apr: Approach){
        approachList.append(apr)
    }
    

    
}
