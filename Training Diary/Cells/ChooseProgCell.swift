//
//  ChooseProgCell.swift
//  Training Diary
//
//  Created by Вова Петров on 15.06.2018.
//  Copyright © 2018 Вова Петров. All rights reserved.
//

import UIKit

class ChooseProgCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desk: UITextView!
    @IBOutlet weak var days: UILabel!
    
}
